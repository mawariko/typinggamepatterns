import * as config from './config';
import { texts } from '../data';
import * as roomStatuses from './room-statuses';
import { Commentator } from './commentator';
import { HistorySingleton } from './history';

export class Room {
  constructor(name, creator, io) {
    this.commentator = new Commentator(io, this);
    this.history = new HistorySingleton();
    this._io = io;
    this.name = name;
    this.users = [];
    this.status = roomStatuses.OPENED;
    this.textId = null;
    this.text = null;

    this.intervalId = null;
    this.secondsLeft = 0;
    this.secondsPast = 0;
    this.gameStartTime = null;

    this._io.emit('addCreatedRoom', this.name);
	creator.socket.emit('letcreatorin');
  }

  addUser(user, isCreator = false) {
    let result = false;

    let usersCount = this.users.length;
    if (usersCount < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      user.reset();

      this.users.push(user);
      usersCount++;
      result = true;

      user.socket.join(this.name);
      user.socket.emit('openRoom', this.name);
      user.socket.emit(
        'enterRoom',
        this.users.map(u => u.name)
      );

      user.socket.in(this.name).emit('enterRoom', [user.name]);
      this._io.emit('numberOfUsers', {
        roomName: this.name,
        usersCount,
      });

	  if(isCreator) {
		this.commentator.greetCreator(user);
	  } else {
	  	this.commentator.greetUser(user);
	  }

      if (usersCount === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        this.status = roomStatuses.FULL;
        this._io.emit('statusRoom', {
          roomName: this.name,
          status: this.status,
        });
      }
    } else {
      this._socket.emit('gameError', 'This room is full');
    }

    return result;
  }

  removeUser(user) {
    this.users = this.users.filter(u => u.name !== user.name);
    this.getSocket().emit('leaveRoom', user.name);
    user.socket.leave(this.name);
    if (this.users.length) {
      this._io.emit('numberOfUsers', {
        roomName: this.name,
        usersCount: this.users.length,
      });
      this.updateRoomStatus();
      this.checkPretermFinish();
    }
  }

  updateRoomStatus() {
    const oldStatus = this.status;
		if(this.status === roomStatuses.OPENED || this.status === roomStatuses.FULL) {
			const everyoneReady = this.users.every(u => u.ready);

			if(everyoneReady && this.users.length > 1) {
				this.initCountdown();
			} else {
				this.status = this.users.length < config.MAXIMUM_USERS_FOR_ONE_ROOM ? roomStatuses.OPENED : roomStatuses.FULL;
			}
		}

		if(this.status !== oldStatus) {
			this._io.emit('statusRoom', {
				roomName: this.name,
				status: this.status
			});
		}
  }

  initCountdown() {
    let textId;
    do {
      textId = Math.floor(Math.random() * texts.length);
    } while (textId === this.textId);

    this.status = roomStatuses.COUNTDOWN;
    this.textId = textId;
    this.text = texts[textId];
    this.secondsLeft = config.SECONDS_TIMER_BEFORE_START_GAME;
    this._io
      .to(this.name)
      .emit('startCountdown', { secondsLeft: this.secondsLeft, textId });
    this.intervalId = setInterval(this.countdownHandler.bind(this), 1000);
    this.commentator.countdownComment();
  }

  initGame() {
    this.status = roomStatuses.RUNNING;
    this.secondsLeft = config.SECONDS_FOR_GAME;
    this.secondsPast = 0;
    this.gameStartTime = new Date();
    this.getSocket().emit('startGame', { secondsLeft: this.secondsLeft });
    this.intervalId = setInterval(this.gameHandler.bind(this), 1000);
    this.commentator.introduction();
  }




  finishGame() {
    clearInterval(this.intervalId);

    this.status =
      this.users.length < config.MAXIMUM_USERS_FOR_ONE_ROOM
        ? roomStatuses.OPENED
        : roomStatuses.FULL;
    this.getSocket().emit('finishGame');
    this._io.emit('statusRoom', {
      roomName: this.name,
      status: this.status,
    });
    this.commentator.informOnResults();
    this.users.forEach((u) => {
      this.history.saveUserResult(u);
      u.reset();
    });
  }

  checkPretermFinish() {
    if (this.text) {
      if (this.users.every(u => u.progress == this.text.length)) {
        this.finishGame();
      }
    }
  }

  // TODO:
  charsBeforeFinish() {
    const finishers = this.users.filter(user => user.progress == this.text.length - 30);
    if (finishers.length) {
      this.commentator.informOn30BeforeFinish(finishers)
    }
  }

  countdownHandler() {
    this.secondsLeft--;
    if (this.secondsLeft > 0) {
      this.getSocket().emit('countdown', this.secondsLeft);
      if (this.secondsLeft == 5) {
        this.commentator.countdownComment2();
      }
      if (this.secondsLeft == 1) {
        this.commentator.countdownComment3();
      }
    } else {
      clearInterval(this.intervalId);
      this.initGame();
    }
  }



  gameHandler() {
    this.secondsLeft--;
    this.secondsPast++;
    if (this.secondsLeft > 0) {
	  this.getSocket().emit('tickGame', this.secondsLeft);
	  if(this.secondsLeft == config.SECONDS_FOR_GAME - 5) {
		  this.commentator.informOnHistory();
	  }
	  if(this.secondsPast % 30 == 0) {
		  this.commentator.informOnProgress();
	  }
    if(this.secondsPast % 30 != 0 
      && this.secondsPast % 10 == 0
      && this.secondsLeft != 0) {
		  this.commentator.tellAJoke();
	  }
     
    } else {
      this.finishGame();
    }
  }

  dispose() {
    if (this.intervalId) {
      clearInterval(this.intervalId);
    }
  }

  getSocket() {
	  return this._io.to(this.name);
  }
}
