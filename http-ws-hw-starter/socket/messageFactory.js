export const MessageTypes = {
    USER: 'UserMessage',
    ROOM: 'RoomMessage',
    JOKE: 'JokeMessage'
};

const EVENT_COMMENTATOR_MESSAGE = 'commentatorMessage';

function* jokesIterator() {
    while(true) {
        yield "My girlfriend and I often laugh about how competitive we are. But I laugh more.";
        yield `How do you keep a programmer in the shower all day?
        Give him a bottle of shampoo which says "lather, rinse, repeat.`;
        yield `- "Have you heard about the object-oriented way to become wealthy?"
        - "No..."
        - "Inheritance."
        `;
        yield `- What is a programmer's favourite hangout place?
        -Foo Bar`;
        yield `How does a computer get drunk? A. It takes screenshots.`
    }
};

export class MessageFactory {
    constructor() {
        this.jokes = jokesIterator();
    }

    create(type, receiver, text) {
        switch (type) {
            case MessageTypes.USER:
                return new UserMessage(receiver, text);
            case MessageTypes.ROOM:
                return new RoomMessage(receiver, text);
            case MessageTypes.JOKE:
                return new JokeMessage(receiver, this.jokes);
        }
    }
}

class UserMessage {
    constructor(user, text) {
        this.user = user;
        this.text = text;
    }

    send() {
        this.user.socket.emit(EVENT_COMMENTATOR_MESSAGE, this.text);
    }
}

class RoomMessage {
    constructor(room, text) {
        this.room = room;
        this.text = text;
    }

    send() {
        this.room.getSocket().emit(EVENT_COMMENTATOR_MESSAGE, this.text);
    }
}

class JokeMessage extends RoomMessage {
    constructor(room, jokes) {
        super(room, `Joke time: \n ${jokes.next().value}`);
    }

}