import {
    MessageFactory,
    MessageTypes
} from './messageFactory';

export class Commentator {
    constructor(io, room) {
        this.io = io;
        this.room = room;
        this.factory = new MessageFactory();
    }

    greetCreator(user) {
        this.factory.create(
            MessageTypes.USER,
            user,
            `Hello, ${user.name}! Thanks for creating this room! Can't wait for the race, but let's wait for at least 1 more person`
        ).send();
    }

    greetUser(user) {
        this.factory.create(
            MessageTypes.USER,
            user,
            `Hi, ${user.name}! Thanks for joining us! I'll introduce myself later, now either hit ready and we roll or let's wait for more people`
        ).send();
    }

    notReadyComment(user) {
        this.factory.create(
            MessageTypes.ROOM,
            this.room,
            `Oh, is it just me or ${user.name} is backing up? Don't be discouraged!`
        ).send();
    }

    readyComment(user) {
        this.factory.create(
            MessageTypes.ROOM,
            this.room,
            `Yey!! ${user.name} is ready for the fight!`
        ).send();
    }

    countdownComment() {
        const names = this.room.users.map((u) => {
            return `${u.name}`;
        }).join(', ');

        this.factory.create(
            MessageTypes.ROOM,
            this.room,
            `${names}, I have to confess...I have a fear of speed bumps. But I am slowly getting over it)`
        ).send();
    }

    countdownComment2() {
        this.factory.create(
            MessageTypes.ROOM,
            this.room,
            'We have no speed bumps here, people! Go at your full speed!'
        ).send();
    }

    countdownComment3() {
        this.factory.create(
            MessageTypes.ROOM,
            this.room,
            'GOOOOO!'
        ).send();
    }

    introduction() {
        this.factory.create(
            MessageTypes.ROOM,
            this.room,
            `It's time to get to know each other.
          I am mr. Snail. Yoroshiku : )
          I'll be updating you on the race
          and tell a joke, here and there.
          So, our contestants are:`
        ).send();
    }

    // TODO: user history

    informOnHistory() {
        const raceHistory = this.room.users.map(user => {
            let message;
            let count = this.room.history.getUserHistory(user).racesCount;
            return message = count == 0 ? `${user.name}: a newcomer! Good luck!` : `${user.name}: has participated in ${count} ${count == 1 ?'race' : 'races'}`
        }).join("\n");

        this.factory.create(
            MessageTypes.ROOM,
            this.room,
            raceHistory
        ).send();
    }

    informOnProgress() {
        const raceProgress = this.room.users.map(user => {
            return `${user.name} has typed ${user.progress} symbols out of ${this.room.text.length}`
        }).join("\n");

        let message = `Ok, we are half way through, racers!
        Let's see who is who and who is where))
        ${raceProgress}
        It's high time you continued at high speed`

        this.factory.create(
            MessageTypes.ROOM,
            this.room,
            message
        ).send();
    }

    informOn30BeforeFinish(finishers) {
        const message = finishers.map(finisher => `Attention!! ${finisher.name} has only 30 chars to go!`).join("\n");

        this.factory.create(
            MessageTypes.ROOM,
            this.room,
            message
        ).send();
    }

    informOnFinish(user) {
        const message = `Congrats!! ${user.name} has finished the race`;

        this.factory.create(
            MessageTypes.ROOM,
            this.room,
            message
        ).send();
    }

    informOnResults() {
        const list = this.room.users
            .sort((a, b) => {
                const progressDiff = b.progress - a.progress;
                return progressDiff !== 0 ?
                    progressDiff :
                    a.lastKeytime - b.lastKeytime;
            })
            .slice(0, 3)
            .map((user, index) => {
                let row = `${index + 1}. ${user.name}: `;
                if (user.progress == this.room.text.length) {
                    const seconds = Math.round((user.lastKeytime - this.room.gameStartTime) / 1000);
                    row += `${seconds} seconds`;
                } else {
                    row += user.progress ?
                        `time is up, you have only ${user.progress} chars out of ${this.room.text.length}` :
                        'looks like you haven\'t moved much';
                }
                return row;
            });

        const message = `FINISH!
        Let's see the results:
        ${list.join('\n')}`;

        this.factory.create(
            MessageTypes.ROOM,
            this.room,
            message
        ).send();
    }

    // TODO: introduce jokes
    tellAJoke() {
        this.factory.create(
            MessageTypes.JOKE,
            this.room
        ).send();
    }

}