import { makeSingleton } from './singletonProxy';

class History {
    constructor() {
        this.usersData = new WeakMap();
    }

    getUserHistory(user) {
        const data = this.usersData.get(user);
        return data ? data : { racesCount: 0 };
    }

    saveUserResult(user) {
        const data = this.getUserHistory(user);
        data.racesCount++;
        this.usersData.set(user, data);
    }
}

export const HistorySingleton = makeSingleton(History);