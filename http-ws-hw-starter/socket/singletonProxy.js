export function makeSingleton(className) {
  let instance = null;
  return new Proxy(className.prototype.constructor, {
    construct: (target, argumentsList) => {
      if (!instance) {
        instance = new target(...argumentsList);
      }
      return instance;
    }
  });
}