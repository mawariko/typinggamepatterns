export class User {
 	constructor(name, socket) {
		this.name = name;
		this.socket = socket;
		this.reset();
	}

	reset() {
		this.ready = false;
		this.progress = 0;
		this.lastKeytime = null;
	}
}