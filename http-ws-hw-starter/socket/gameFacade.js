import { User } from './user';
import { Room } from './room';

const rooms = [];
const userNames = [];

export class Game {
  constructor(io, socket) {
    this.io = io;
    this.socket = socket;
    this.currentUser = null;
    this.currentRoom = null;
  }

  registerUser() {
    let result = false;
    const username = this.socket.handshake.query.username;
    if (userNames.includes(username)) {
      this.socket.emit('userPresent', username);
    } else {
      this.currentUser = new User(username, this.socket);
      userNames.push(username);
      result = true;
    }

    return result;
  }

  renderRooms() {
    if (this.currentUser && rooms.length) {
      this.socket.emit(
        'renderRooms',
        rooms.map(room => ({
          name: room.name,
          usersCount: room.users.length,
          status: room.status,
        }))
      );
    }
  }

  createRoom(roomName) {
    if (!roomName) {
      this.socket.emit('gameError', 'Invalid room name');
      return;
    }
    if (rooms.some(room => room.name === roomName)) {
      this.socket.emit('gameError', 'Room with this name already exits');
      return;
    }

    this.currentRoom = new Room(roomName, this.currentUser, this.io);

    rooms.push(this.currentRoom);
  }

  addCreatorToRoom() {
    if (this.currentRoom) {
      this.currentRoom.addUser(this.currentUser, true);
    }
  }

  addUserToRoom(roomName) {
    const room = rooms.find(room => room.name === roomName);
    if (room && room.addUser(this.currentUser)) {
      this.currentRoom = room;
    }
    const statuses = this.currentRoom.users.map(user => ({
      userName: user.name,
      status: user.ready,
    }));
    this.io.to(this.currentRoom.name).emit('statusUser', statuses);
  }

  tooggleStatusUser() {
    if (this.currentRoom) {
      this.currentUser.ready = !this.currentUser.ready;
      this.currentUser.ready
        ? this.currentRoom.commentator.readyComment(this.currentUser)
        : this.currentRoom.commentator.notReadyComment(this.currentUser);
      this.io.to(this.currentRoom.name).emit('statusUser', [
        {
          userName: this.currentUser.name,
          status: this.currentUser.ready,
        },
      ]);
    }
  }

  updateStatusRoom() {
    this.currentRoom.updateRoomStatus();
  }

  trackProgress(count) {
    const progressPercent = Math.floor(
      (100 * count) / this.currentRoom.text.length
    );

    this.currentUser.progress = count;
    this.currentUser.lastKeytime = new Date();

    if(this.currentUser.progress == this.currentRoom.text.length) {
      this.currentRoom.commentator.informOnFinish(this.currentUser);
    }
    this.io.to(this.currentRoom.name).emit('progressPercentUser', {
      userName: this.currentUser.name,
      progressPercent,
    });
    this.currentRoom.charsBeforeFinish()
    this.currentRoom.checkPretermFinish();
  }

  disconnectUser() {
    const index = userNames.indexOf(this.currentUser.name);
    if (index !== -1) {
      userNames.splice(index, 1);
      this.removeUserFromRoom();
    }
  }

  removeUserFromRoom() {
    if (this.currentRoom) {
      this.currentRoom.removeUser(this.currentUser);
      if (this.currentRoom.users.length) {
        this.updateStatusRoom();
      } else {
        this.currentRoom.dispose();
        const index = rooms.indexOf(this.currentRoom);
        if (index !== -1) {
          rooms.splice(index, 1);
        }
        this.io.emit('deleteRoom', this.currentRoom.name);
      }
      this.currentRoom = null;
    }
  }

}
