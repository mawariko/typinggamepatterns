const ROOM_STATUS_OPENED = 'opened';

const currentUserName = sessionStorage.getItem("username");
const createRoomBtn = document.getElementById('create-room');
const readyBtn = document.getElementById('ready-btn');
const countdown = document.getElementById('countdown');
const roomsPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');

const roomDetails = document.querySelector('.room-details');
const usersContainer = document.querySelector('.users-container');
const rooms = document.querySelector('.rooms');
const secondsLeftBox = document.querySelector('.seconds-left');
const text = document.getElementById('text');

//  for comments 

const coomentatorSpeech = document.querySelector('.commentator-speech');
//  for comments 

let gameText = null;
let gameTextChars = [];
let gameCurrentIndex = null;

if (!currentUserName) {
  window.location.replace("/login");
}

const socket = io("", {
  query: {
    username: currentUserName
  }
});

const onJoinBtnClick = e => {
  let roomname = e.target.getAttribute('data-name');
  console.log(roomname);
  socket.emit('join', roomname);
};

const createRoomCard = (room, usersCount = 1, ststus = ROOM_STATUS_OPENED) => {
  const card = document.createElement('div');

  card.setAttribute('id', `room-${room}`);
  card.setAttribute('class', 'room-card');
  if (ststus !== ROOM_STATUS_OPENED) {
    card.classList.add('display-none');
  }
  card.innerHTML = `
    <div class="members"><span class="count">${usersCount}</span> <span class="unit">${usersCount == 1 ? 'user' : 'users'}</span> connected</div>
    <h2 class="room-name">${room}</h2>
    <a class="btn btn-join" data-name="${room}">Join</a>
  `;
  rooms.appendChild(card);
  card.querySelector('.btn-join').addEventListener("click", onJoinBtnClick);
};

const gameKeyPress = e => {
  if (e.key == gameText[gameCurrentIndex]) {
    const currnetChar = gameTextChars[gameCurrentIndex];
    currnetChar.classList.remove('current');
    currnetChar.classList.add('typed');
    if (gameCurrentIndex < gameText.length - 1) {
      gameTextChars[gameCurrentIndex + 1].classList.add('current');
    } else {
      document.removeEventListener('keypress', gameKeyPress);
    }

    gameCurrentIndex++;

    socket.emit('progress', gameCurrentIndex);
  }
}

socket.on('userPresent', () => {
  sessionStorage.clear();
  alert(`name "${currentUserName}" is already taken`);
  window.location.replace("/login");
});

// EVENT LISTENERS
createRoomBtn.addEventListener('click', () => {
  const roomName = prompt("How do you want to name your room?");
  if (roomName) {
    socket.emit('create', roomName);
  }
});

readyBtn.addEventListener('click', () => socket.emit('toggleStatusUser'));

// End Event Listeners
//display all rooms
socket.on('renderRooms', (roomList) => {
  roomList.map(({
    name,
    usersCount,
    status
  }) => createRoomCard(name, usersCount, status));
})

socket.on('addCreatedRoom', (room) => {
  createRoomCard(room);
})

socket.on('letcreatorin', () => {
  socket.emit('creatorjoin');
});

socket.on('numberOfUsers', ({
  roomName,
  usersCount
}) => {
  const counter = document.querySelector(`#room-${roomName} .count`);
  const unitLabel = document.querySelector(`#room-${roomName} .unit`);
  counter.textContent = usersCount;
  unitLabel.textContent = usersCount == 1 ? 'user' : 'users';
});


socket.on('openRoom', roomName => {
  roomsPage.classList.add('display-none');
  gamePage.classList.remove('display-none');
  const roomHeaderHtml = ` <h1>${roomName}</h1>
  <button id="back-to-rooms" href="#" class="btn"> back to rooms </button>`;
  roomDetails.innerHTML = roomHeaderHtml;
});


socket.on('enterRoom', roomUsers => {
  roomUsers.map(userName => {
    let userHtml = `
    <div class="user-progress" id="progress-${userName}">
        <div>
          <div class="user-status"></div>
          <label for="progress">${userName}${userName === currentUserName ? ' (you)' : ''}</label>
        </div>
        <progress id="progress" min="0" max="100" value="0"></progress> 
    </div>
    `
    usersContainer.innerHTML += userHtml;
  })
  roomDetails.append(usersContainer);
  const backToRooms = document.getElementById('back-to-rooms');
  backToRooms.addEventListener('click', () => {
    socket.emit('goToRooms');
  });
});

socket.on('leaveRoom', userName => {
  if (userName === currentUserName) {
    roomsPage.classList.remove('display-none');
    gamePage.classList.add('display-none');
    usersContainer.innerHTML = '';
  } else {
    const userProgress = document.getElementById(`progress-${userName}`);
    if (userProgress) {
      userProgress.remove();
    }
  }
});

socket.on('statusRoom', ({
  roomName,
  status
}) => {
  const card = document.getElementById(`room-${roomName}`);
  if (card) {
    if (status === ROOM_STATUS_OPENED) {
      card.classList.remove('display-none');
    } else {
      card.classList.add('display-none');
    }
  }
});

socket.on('deleteRoom', roomName => {
  const card = document.getElementById(`room-${roomName}`);
  if (card) {
    card.remove();
  }
});

socket.on('statusUser', (statuses) => {
  statuses.map(st => {
    const userStatus = document.querySelector(`#progress-${st.userName} .user-status`);
    if (userStatus) {
      if (st.status) {
        userStatus.classList.add('ready');
      } else {
        userStatus.classList.remove('ready');
      }
    }
    if (st.userName === currentUserName) {
      readyBtn.innerText = status ? 'Not ready' : 'Ready';
    }
  })
 
  
});

socket.on('progressPercentUser', ({ userName, progressPercent }) => {
  const userLabel = document.querySelector(`#progress-${userName} label`);
  const userProgress = document.querySelector(`#progress-${userName} progress`);
  if (userProgress){
    userProgress.setAttribute('value', progressPercent);
    if (progressPercent == 100) {
      userLabel.classList.add('finished');
    }
  }
});

socket.on('startCountdown', ({
  secondsLeft,
  textId
}) => {
  countdown.innerText = secondsLeft;

  document.getElementById('back-to-rooms').classList.add('display-none');
  readyBtn.classList.add('display-none');
  countdown.classList.remove('display-none');

  fetch(`/game/texts/${textId}`).then(response => response.text().then(text => {
    gameText = text;
  }));
});

socket.on('countdown', secondsLeft => countdown.innerText = secondsLeft);

socket.on('startGame', ({
  secondsLeft
}) => {
  gameCurrentIndex = 0;

  secondsLeftBox.querySelector('.value').innerText = secondsLeft;
  secondsLeftBox.querySelector('.unit').innerText = secondsLeft == 1 ? 'second' : 'seconds';

  text.innerHTML = '';
  gameTextChars = [];
  gameText.split('').forEach(char => {
    const span = document.createElement('span');
    span.innerText = char;
    gameTextChars.push(span);
    text.appendChild(span);
  });
  text.querySelector('span:first-child').classList.add('current');

  countdown.classList.add('display-none');
  secondsLeftBox.classList.remove('display-none');
  text.classList.remove('display-none');

  document.addEventListener('keypress', gameKeyPress);
});

socket.on('finishGame', () => {
  document.querySelectorAll(`.user-progress .user-status`).forEach(el => el.classList.remove('ready'));
  document.querySelectorAll(`.user-progress label`).forEach(el => el.classList.remove('finished'));
  document.querySelectorAll(`.user-progress progress`).forEach(el => el.setAttribute('value', 0));
  readyBtn.innerText = 'Ready';

  document.removeEventListener('keypress', gameKeyPress);
  secondsLeftBox.classList.add('display-none');
  text.classList.add('display-none');
  document.getElementById('back-to-rooms').classList.remove('display-none');
  readyBtn.classList.remove('display-none');
});

socket.on('tickGame', secondsLeft => {
  secondsLeftBox.querySelector('.value').innerText = secondsLeft;
  secondsLeftBox.querySelector('.unit').innerText = secondsLeft == 1 ? 'second' : 'seconds';
});

socket.on('gameError', message => alert(message));

// COMMENTATOR
socket.on('commentatorMessage', message => showComment(message));
socket.on('hey', message => console.log(message));


const showComment = (message) => {
  coomentatorSpeech.innerText = message;
}