import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import { texts } from "../data";

const router = Router();

router
  .get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  })
  .get("/texts/:id", (req, res) => {
    const id = req.params.id;
    if(id < texts.length) {
      res.set('Content-Type', 'text/plain').send(texts[id]);
    } else {
      res.status(404).send('Not found');
    }
  });

export default router;
