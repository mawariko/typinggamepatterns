export const texts = [
  "Lunch came just as they were off Sheerness.  He didn't feel so hungry as he thought he should, and so contented himself with a bit of boiled beef, and some strawberries and cream.",
  "Meanwhile the third man, who has been baling out the boat, and who has spilled the water down his sleeve, and has been cursing away to himself steadily for the last ten minutes...",
  "My friend got rid of them, at last, by taking them down to a sea-side town, and burying them on the beach.  It gained the place quite a reputation.",
  "And then it was George's turn, and he trod on the butter.  I didn't say anything, but I came over and sat on the edge of the table and watched them.  It irritated them more than anything I could have said.",
  "There seemed a good deal of luggage, when we put it all together.  There was the Gladstone and the small hand-bag, and the two hampers, and a large roll of rugs, and some four or five overcoats and macintoshes, and a few umbrellas, and then there was a melon by itself in a bag...",
  "No, what was sad in his case was that he, who didn't care for carved oak, should have his drawing-room panelled with it, while people who do care for it have to pay enormous prices to get it.",
];


export default { texts };
